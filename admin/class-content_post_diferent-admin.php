<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/admin
 * @author     Luis And Juan <luiscapote30@gmail.com>
 */
class Content_post_diferent_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of this plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/content_post_diferent-admin.css', array(), $this->version, 'all');

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {


		if ((isset($_GET['post_type']) && $_GET['post_type'] == 'gallery') ||
	 			(isset($_GET['post_type']) && $_GET['post_type'] == 'videos') ||
				(isset($_GET['post_type']) && $_GET['post_type'] == 'infografiaglobal') ||
				(isset($_GET['post_type']) && $_GET['post_type'] == 'programaexpositivo') ||
				(isset($_GET['post_type']) && $_GET['post_type'] == 'programaeditorial')) {
			wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/content_post_diferent-admin-otros.js', array('jquery'), $this->version, false);
		}

		
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/content_post_diferent-admin-banner.js', array('jquery'), $this->version, false);
		wp_enqueue_media();
		if (isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'category') {
        wp_enqueue_media();
        wp_register_script('admin-js', plugin_dir_url( __FILE__ ) . 'js/content_post_diferent-admin.js', array( 'jquery' ), $this->version);  // Esto en el caso de que estemos en un plugin; si estamos en un Theme, hay que modificar la ruta de acceso, estableciendo la del Theme.
        wp_enqueue_script('admin-js');
    	}

		if (isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'post_tag') {
	        wp_enqueue_media();
	        wp_register_script('admin-js', plugin_dir_url( __FILE__ ) . 'js/content_post_diferent-admin.js', array( 'jquery' ), $this->version);  // Esto en el caso de que estemos en un plugin; si estamos en un Theme, hay que modificar la ruta de acceso, estableciendo la del Theme.
	        wp_enqueue_script('admin-js');
	    }
	}


	/**
	 * @param $meta_boxes
	 *
	 * @return array
	 */
	function video_url_get_meta_box( $meta_boxes ) {
		$prefix       = 'video-';
		$meta_boxes[] = array(
			'id'         => 'video',
			'title'      => esc_html__( 'Video URL', 'metabox-online-generator' ),
			'post_types' => array( 'programapapagayo', 'programasintegracion' ),
			'context'    => 'advanced',
			'priority'   => 'default',
			'autosave'   => true,
			'fields'     => array(
				array(
					'id'          => $prefix . 'text_1',
					'type'        => 'text',
					'name'        => esc_html__( 'Url', 'metabox-online-generator' ),
					'placeholder' => esc_html__( 'Url Del Video', 'metabox-online-generator' ),
					'clone'       => true,
					'max_clone'   => 5,
					'add_button'  => esc_html__( 'Añadir Videos', 'metabox-online-generator' ),
					'sort_clone'  => true,
				),
				array(
					'id'          => $prefix . 'text_2',
					'type'        => 'text',
					'name'        => esc_html__( 'Titulo', 'metabox-online-generator' ),
					'placeholder' => esc_html__( 'Titulo Del Video', 'metabox-online-generator' ),
					'clone'       => true,
					'max_clone'   => 5,
					'add_button'  => esc_html__( 'Añadir Titulo', 'metabox-online-generator' ),
					'sort_clone'  => true,
				),
			),
		);

		return $meta_boxes;
	}

// Register Custom Post Type Videos
// Post Type Key: videos
	function create_videos_cpt() {

		$labels = array(
			'name'                  => __('Videos', 'Post Type General Name', 'textdomain'),
			'singular_name'         => __('Videos', 'Post Type Singular Name', 'textdomain'),
			'menu_name'             => __('Videos', 'textdomain'),
			'name_admin_bar'        => __('Videos', 'textdomain'),
			'archives'              => __('Videos Archivos', 'textdomain'),
			'attributes'            => __('Videos Attributos', 'textdomain'),
			'parent_item_colon'     => __('Parent Videos:', 'textdomain'),
			'all_items'             => __('Mostrar todo', 'textdomain'),
			'add_new_item'          => __('Añadir Nuevo', 'textdomain'),
			'add_new'               => __('Añadir Nuevo', 'textdomain'),
			'new_item'              => __('Nuevo Videos', 'textdomain'),
			'edit_item'             => __('Editar Videos', 'textdomain'),
			'update_item'           => __('Actualizar Videos', 'textdomain'),
			'view_item'             => __('Ver Videos', 'textdomain'),
			'view_items'            => __('Ver Todos los videos', 'textdomain'),
			'search_items'          => __('Buscar Videos', 'textdomain'),
			'not_found'             => __('Not found', 'textdomain'),
			'not_found_in_trash'    => __('Not found in Trash', 'textdomain'),
			'featured_image'        => __('Featured Image', 'textdomain'),
			'set_featured_image'    => __('Set featured image', 'textdomain'),
			'remove_featured_image' => __('Remove featured image', 'textdomain'),
			'use_featured_image'    => __('Use as featured image', 'textdomain'),
			'insert_into_item'      => __('Insert into Videos', 'textdomain'),
			'uploaded_to_this_item' => __('Uploaded to this Videos', 'textdomain'),
			'items_list'            => __('Custom Posts list', 'textdomain'),
			'items_list_navigation' => __('Custom Posts list navigation', 'textdomain'),
			'filter_items_list'     => __('Filter Custom Posts list', 'textdomain'),
		);
		$args   = array(
			'label'               => __('Videos', 'textdomain'),
			'description'         => __('', 'textdomain'),
			'labels'              => $labels,
			'menu_icon'           => 'dashicons-format-video',
			'supports'            => array('title', 'editor', 'author','taxonomies','thumbnail'),
			'taxonomies'          => array('category'),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 10,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'hierarchical'        => false,
			'exclude_from_search' => false,
			'show_in_rest'        => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type('videos', $args);

	}



	




	function categorias_add_new_meta_fields(){
		?>
		<div>
				<label for="term_meta[imagen]">
				<input type="text" name="term_meta[imagen]" size="36" id="upload_image" value=""><br>
				<input id="upload_image_button" type="button" class='button button-primary' value="Subir Imagen" />
				<br/><i>Introduce una URL o establece una imagen para este campo.</i>
				</label>
		</div>
		<?php
	}

	

	function categorias_edit_meta_fields($term){
		$t_id = $term->term_id;

		$term_meta = get_option("taxonomy_$t_id");
		?>
			<tr valign="top" class='form-field'>
				<th scope="row">Subir imagen</th>
				<td>
					<label for="upload_image">
						<input id="upload_image" type="text" size="36" name="term_meta[imagen]" value="<?php if( esc_attr( $term_meta['imagen'] ) != "") echo esc_attr( $term_meta['imagen'] ) ; ?>" />
						<p><input id="upload_image_button" type="button" class='button button-primary' style='width: 100px' value="Subir Imagen" />
						<i>Introduce una URL o establece una imagen para este campo.</i></p>
					</label>
					<p><?php if( esc_attr( $term_meta['imagen'] ) != "" ) echo "<table><tr><td><i><strong>Imagen actual</strong></i>:</td><td> <img style='width:30%' src='".esc_attr( $term_meta['imagen'] )."'></td></tr></table>"; ?></p>
				</td>
			</tr>
		<?php
	}


	function categorias_save_custom_meta( $term_id ) {
		if ( isset( $_POST['term_meta'] ) ) {
			$t_id = $term_id;
			$term_meta = get_option( "taxonomy_$t_id" );
			$cat_keys = array_keys( $_POST['term_meta'] );
			foreach ( $cat_keys as $key ) {
				if ( isset ( $_POST['term_meta'][$key] ) ) {
					$term_meta[$key] = $_POST['term_meta'][$key];
				}
			}
			update_option( "taxonomy_$t_id", $term_meta );
		}
	}

	// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
	// Submenu Page
	// add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
	public function add_menu() {

		add_submenu_page(
			'edit.php?post_type=gallery',
			'Configuración',
			'Configuración',
			'manage_options',
			'options-gallery',
			array($this, 'page_configu')
		);
	}

	public function page_configu() {
		include(plugin_dir_path(__FILE__) . 'partials/page_configuration.php');
	}

	public function register_setting_field() {
		register_setting('page_configuracion', 'tamano', 'strval');
		register_setting('page_configuracion', 'id_galeria_foto', 'strval');
		register_setting('page_configuracion', 'id_foto_portada_gallery', 'strval');
		register_setting('page_configuracion', 'descrip_imagen_gallery', 'strval');
	}

	public function add_menu_video() {

		add_submenu_page(
			'edit.php?post_type=videos',
			'Configuración',
			'Configuración',
			'manage_options',
			'options-videos',
			array($this, 'page_configu_videos')
		);
	}

	public function page_configu_videos() {
		include(plugin_dir_path(__FILE__) . 'partials/page_configuration_videos.php');
	}

	public function register_setting_field_videos() {
		register_setting('page_configuracion_videos', 'id_foto_portada_videos', 'strval');
		register_setting('page_configuracion_videos', 'descrip_imagen_videos', 'strval');

		register_setting('page_configuracion_videos', 'id_cat_videosexpositivos', 'strval');
	}

	


	public function register_menu_content() {
		register_nav_menus(array(
		
			
			'pdatos'     => esc_html__('P. Datos', 'bbva-com'),
			'cintillopdatos'  => esc_html__('Cintillo P. Datos', 'bbva-com'),
			'basesconsurso'  => esc_html__('Bases del Consurso', 'bbva-com'),
			'quienessomospapagayo'  => esc_html__('Quienes Somos Papagayo', 'bbva-com'),
		));
	}

}
