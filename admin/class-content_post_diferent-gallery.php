<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/admin
 * @author     Luis And Juan <luiscapote30@gmail.com>
 */
class Content_post_diferent_Gallery {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of this plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

// Register Custom Post Type Videos
// Post Type Key: videos
	function create_gallery_cpt() {

		$labels = array(
			'name'                  => __('Galerías', 'Post Type General Name', 'textdomain'),
			'singular_name'         => __('Galería', 'Post Type Singular Name', 'textdomain'),
			'menu_name'             => __('Galería', 'textdomain'),
			'name_admin_bar'        => __('Galería', 'textdomain'),
			'archives'              => __('Galería Archivos', 'textdomain'),
			'attributes'            => __('Galería Attributos', 'textdomain'),
			'parent_item_colon'     => __('Parent Galería:', 'textdomain'),
			'all_items'             => __('Mostrar Todas Galerias', 'textdomain'),
			'add_new_item'          => __('Añadir Nuevas Galerias', 'textdomain'),
			'add_new'               => __('Añadir Nuevas Galerias', 'textdomain'),
			'new_item'              => __('Nueva Galería', 'textdomain'),
			'edit_item'             => __('Editar Galería', 'textdomain'),
			'update_item'           => __('Actualizar Galería', 'textdomain'),
			'view_item'             => __('Ver Galerías', 'textdomain'),
			'view_items'            => __('Ver Todos las Galerías', 'textdomain'),
			'search_items'          => __('Buscar Galería', 'textdomain'),
			'not_found'             => __('Not found', 'textdomain'),
			'not_found_in_trash'    => __('Not found in Trash', 'textdomain'),
			'featured_image'        => __('Image De Portada', 'textdomain'),
			'set_featured_image'    => __('Cambiar imagen', 'textdomain'),
			'remove_featured_image' => __('Remover imagen', 'textdomain'),
			'use_featured_image'    => __('Use as featured image', 'textdomain'),
			'insert_into_item'      => __('Insert into Gallery', 'textdomain'),
			'uploaded_to_this_item' => __('Uploaded to this Videos', 'textdomain'),
			'items_list'            => __('Custom Posts list', 'textdomain'),
			'items_list_navigation' => __('Custom Posts list navigation', 'textdomain'),
			'filter_items_list'     => __('Filter Custom Posts list', 'textdomain'),
		);
		$args   = array(
			'label'               => __('Videos', 'textdomain'),
			'description'         => __('', 'textdomain'),
			'labels'              => $labels,
			'menu_icon'           => 'dashicons-images-alt2',
			'supports'            => array('title', 'author', 'thumbnail'),
			'taxonomies'          => array('gallery'),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 10,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'hierarchical'        => false,
			'exclude_from_search' => false,
			'show_in_rest'        => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);

		register_post_type('gallery', $args);
	}

	/**
	 * @param $defaults
	 *
	 * @return mixed
	 */
	function add_columns_head_only_gallery($defaults) {
		$date = $defaults['date'];
		unset($defaults['date']);
		$author = $defaults['author'];
		unset($defaults['author']);

//		$defaults['featured_image'] = 'Featured Image';
		$defaults['author']     = $author;
		$defaults['id_gallery'] = 'ID Galería';
		$defaults['date']       = $date;

		return $defaults;
	}


	function my_admin_custom_styles() {
		$output_css = '<style type="text/css">
        .column-id_gallery { width: 10% }
    	</style>';
		echo $output_css;
	}

	function add_columns_content_only_gallery($column_name, $post_ID) {
		switch($column_name) {
			case 'featured_image' :
				echo get_the_post_thumbnail($post_ID, 'featured_preview');
				break;
			case 'id_gallery' :
				echo '<span>' . $post_ID . '<span/>';
				break;
		}
	}


	function my_sortable_cake_column($defaults) {
		$defaults['categoria'] = 'Categoría';

		return $defaults;
	}

	/**
	 * @param $query
	 */
	function my_slice_orderby($query) {
		if( ! is_admin()) {
			return;
		}

		$orderby = $query->get('orderby');

		if('categoria' == $orderby) {
			$query->set('meta_key', 'categoria');
			$query->set('orderby', 'meta_value');
		}
	}

	/**
	 * @param $meta_boxes
	 *
	 * @return array
	 */
	function multiple_image_get_meta_box($meta_boxes) {
		$prefix = 'prefix-';

		$meta_boxes[] = array(
			'id'         => 'galley_image',
			'title'      => esc_html__('Subir Imagenes', 'metabox-online-generator'),
			'post_types' => array('gallery'),
			'context'    => 'advanced',
			'priority'   => 'default',
			'autosave'   => true,
			'fields'     => array(
				array(
					'id'         => $prefix . 'image_advanced_1',
					'type'       => 'image_advanced',
					'name'       => esc_html__('Imagen Gallery', 'metabox-online-generator'),
					'clone'      => true,
					'max_status' => false,
					'sort_clone' => true,
				),
			),
		);

		return $meta_boxes;
	}


}
