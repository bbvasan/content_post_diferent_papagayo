jQuery(function($) {
    // $(document).ready(function() {
    //     $('#insert-my-media').click(open_media_window);
    // });
    

    var custom_uploader;

    $('#upload_image_button').click(function(e) {

        e.preventDefault();

        // Si el objeto Uploader ya ha sido creado, reabrimos el cuadro de dialogo.
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        // Extendemos del objeto wp.media
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Usar Imagen',
            button: {
                text: 'Usar Imagen'
            },
            multiple: false
        });

        // Cuando añadimos un archivo, se atrapa la URL y se manda al campo de texto #upload_image.
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#upload_image').val(attachment.url);
        });

        // Abre el cuadro de dialogo
        custom_uploader.open();

    });

    // function open_media_window() {
    //     if (this.window === undefined) {
    //         this.window = wp.media({
    //             title: 'Insert a media',
    //             library: { type: 'image' },
    //             multiple: false,
    //             button: { text: 'Insert' }
    //         });

    //         var self = this; // Needed to retrieve our variable in the anonymous function below
    //         this.window.on('select', function() {
    //             var first = self.window.state().get('selection').first().toJSON();
    //             wp.media.editor.insert('[banner id="' + first.id + '"]');
    //         });
    //     }

    //     this.window.open();
    //     return false;
    // }
});
