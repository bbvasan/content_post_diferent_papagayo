<?php
/**
 * Created by PhpStorm.
 * User: ve07985
 * Date: 28/08/2017
 * Time: 03:52 PM
 */
?>

	
    <h2>Imagen de cabecera</h2>

    <?php wp_enqueue_media(); update_option( 'media_selector_attachment_id', absint( $_POST['image_attachment_id'] ) ); ?>
	<form method='post'>
		<!--<div class='image-preview-wrapper'>
			<img id='image-preview' src='<?php echo wp_get_attachment_url( get_option( 'media_selector_attachment_id' ) ); ?>' height='100'>
		</div>-->
		<input id="upload_image_button" type="button" class="button" value="<?php _e( 'Select image' ); ?>" />
		<input type="visible" name="image_attachment_id" id="image_attachment_id" value="<?php echo get_option( 'media_selector_attachment_id' ); ?>">        
	</form>

<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
<div class="wrap">
    <form method="post" action="options.php">
		<?php
		settings_fields( 'page_configuracion' );
		do_settings_sections( 'page_configuracion' );
		?>
        <table class="form-table"> 
            <tbody class="form-table">
            <tr>
                <th scope="row">
                    <label for="tamano"> Tamaño de la Imagen</label>
                </th>
                <td>
                    <input type="text"
                           value="<?php echo get_option( 'tamano' ); ?>"
                           name="tamano"
                           placeholder=""
                           class="regular-text">
                    <p class="description" id="tagline-description">
                        Tamaño de la imagen Large, Medium, Thumbnail
                    </p>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="id_galeria_foto"> ID de La Galería </label>
                </th>
                <td>
                    <input type="text"
                           value="<?php echo get_option( 'id_galeria_foto' ); ?>"
                           name="id_galeria_foto"
                           placeholder=""
                           class="regular-text">
                    <p class="description" id="tagline-description">
                        Coloca el ID de la Galería, el cual puedes ver en el Enlace Mostrar Todas Galerías
                    </p>
                </td>
            </tr>
            <tr>
            <th scope="row">
                    <label for="id_foto_portada_gallery"> ID de Imagen Cabecera </label>
                </th>
                <td>
                    <input type="text"
                           value="<?php echo get_option( 'id_foto_portada_gallery' ); ?>"
                           name="id_foto_portada_gallery"
                           placeholder=""
                           class="regular-text">
                    <p class="description" id="tagline-description">
                        Coloca el ID de la imagen de cabecera, el cual puedes ver luego de seleccionar la imagen
                    </p>
                    <img src='<?php echo wp_get_attachment_url( get_option( 'id_foto_portada_gallery' ) ); ?>' height='100'>
                </td>
            </tr>
            <tr>
            <th scope="row">
                    <label for="descrip_imagen_gallery"> Descripcion de la seccion Galería </label>
                </th>
                <td>
                    <input type="text"
                           value="<?php echo get_option( 'descrip_imagen_gallery' ); ?>"
                           name="descrip_imagen_gallery"
                           placeholder=""
                           class="regular-text">
                    <p class="description" id="tagline-description">
                        Coloca la descripcion que va luego de la Imagen Cabecera 
                    </p>
                </td>
            </tr>
            </tbody>
            </table>
		<?php submit_button( 'Save Settings' ); ?>
    </form>
</div>

<?php  



