<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              content_post_diferent
 * @since             1.0.0
 * @package           Content_post_diferent
 *
 * @wordpress-plugin
 * Plugin Name:       content_post_diferent
 * Plugin URI:        content_post_diferent
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Luis And Juan
 * Author URI:        content_post_diferent
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       content_post_diferent
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-content_post_diferent-activator.php
 */
function activate_content_post_diferent() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-content_post_diferent-activator.php';
	Content_post_diferent_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-content_post_diferent-deactivator.php
 */
function deactivate_content_post_diferent() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-content_post_diferent-deactivator.php';
	Content_post_diferent_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_content_post_diferent' );
register_deactivation_hook( __FILE__, 'deactivate_content_post_diferent' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-content_post_diferent.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_content_post_diferent() {

	$plugin = new Content_post_diferent();
	$plugin->run();

}
run_content_post_diferent();
