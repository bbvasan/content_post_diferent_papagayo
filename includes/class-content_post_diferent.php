<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/includes
 * @author     Luis And Juan <luiscapote30@gmail.com>
 */
class Content_post_diferent {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Content_post_diferent_Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'content_post_diferent';
		$this->version     = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Content_post_diferent_Loader. Orchestrates the hooks of the plugin.
	 * - Content_post_diferent_i18n. Defines internationalization functionality.
	 * - Content_post_diferent_Admin. Defines all hooks for the admin area.
	 * - Content_post_diferent_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-content_post_diferent-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-content_post_diferent-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-content_post_diferent-admin.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-content_post_diferent-metabox.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-content_post_diferent-gallery.php';
		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-content_post_diferent-public.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'public/menu_walker.php';

		$this->loader = new Content_post_diferent_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Content_post_diferent_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Content_post_diferent_i18n();

		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin  = new Content_post_diferent_Admin($this->get_plugin_name(), $this->get_version());
		$plugin_admin2 = new Content_post_diferent_Gallery($this->get_plugin_name(), $this->get_version());

		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');


		
		
		
		$this->loader->add_action('init', $plugin_admin, 'create_videos_cpt');




		
		
	
	
	
		
		
		//HOME PAPAGAYO
		//$this->loader->add_action( 'init', $plugin_admin, 'create_homepapagayo_cpt');




		$this->loader->add_action('init', $plugin_admin, 'register_menu_content');
		
		// $this->loader->add_filter('rwmb_meta_boxes', $plugin_admin, 'formulario_get_meta_box');
		// $this->loader->add_filter('rwmb_meta_boxes', $plugin_admin, 'cursos_get_meta_box');

	
		$this->loader->add_action( 'category_add_form_fields', $plugin_admin, 'categorias_add_new_meta_fields', 10, 2 );
		$this->loader->add_action( 'category_edit_form_fields', $plugin_admin, 'categorias_edit_meta_fields', 10, 2 );
		$this->loader->add_action( 'edited_category', $plugin_admin, 'categorias_save_custom_meta', 10, 2 );
		$this->loader->add_action( 'create_category', $plugin_admin, 'categorias_save_custom_meta', 10, 2 );

		$this->loader->add_action( 'post_tag_add_form_fields', $plugin_admin, 'categorias_add_new_meta_fields', 10, 2 );
		$this->loader->add_action( 'post_tag_edit_form_fields', $plugin_admin, 'categorias_edit_meta_fields', 10, 2 );
		$this->loader->add_action( 'edited_post_tag', $plugin_admin, 'categorias_save_custom_meta', 10, 2 );
		$this->loader->add_action( 'create_post_tag', $plugin_admin, 'categorias_save_custom_meta', 10, 2 );




		//Papagayo Videos
		$this->loader->add_filter('rwmb_meta_boxes', $plugin_admin, 'video_url_get_meta_box');

		//Configuracion pagina para gallery
		$this->loader->add_action('admin_menu', $plugin_admin, 'add_menu');
		$this->loader->add_action('admin_init', $plugin_admin, 'register_setting_field');

		//Configuracion pagina para videos
		$this->loader->add_action('admin_menu', $plugin_admin, 'add_menu_video');
		$this->loader->add_action('admin_init', $plugin_admin, 'register_setting_field_videos');

		


		//gallery
		$this->loader->add_action('pre_get_posts', $plugin_admin2, 'my_slice_orderby');
		$this->loader->add_filter('manage_edit-gallery_sortable_columns', $plugin_admin2, 'my_sortable_cake_column');
		$this->loader->add_filter('rwmb_meta_boxes', $plugin_admin2, 'multiple_image_get_meta_box');
		$this->loader->add_filter('manage_gallery_posts_columns', $plugin_admin2, 'add_columns_head_only_gallery');
		$this->loader->add_action('manage_gallery_posts_custom_column', $plugin_admin2, 'add_columns_content_only_gallery', 10, 2);
		$this->loader->add_action('init', $plugin_admin2, 'create_gallery_cpt');
		$this->loader->add_action('admin_head', $plugin_admin2, 'my_admin_custom_styles');


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Content_post_diferent_Public($this->get_plugin_name(), $this->get_version());

		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
		$this->loader->add_action('init', $plugin_public, 'register_shortcodes');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Content_post_diferent_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
