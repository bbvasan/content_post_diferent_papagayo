<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/public
 * @author     Luis And Juan <luiscapote30@gmail.com>
 */
class Content_post_diferent_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style('fancybox', plugin_dir_url( __FILE__ ) . 'css/jquery.fancybox.css', array(), $this->version, 'all' );
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/content_post_diferent-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_media();

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/content_post_diferent-public.js', array('jquery'), true, true);
		wp_enqueue_script('formulario', plugin_dir_url(__FILE__) . 'js/formulario.js', array('jquery'), true, true);

	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_papagayo() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-papagayo.php';
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_saladeprensa() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-prensa.php';
	}

	public function load_frontend_page_edufinanciera() {
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-edufinanciera.php';
	}

	public function load_frontend_page_educate() {
		require plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-financiera-educate.php';
	}
	public function load_frontend_page_herramienta() {
		require plugin_dir_path(__FILE__) . 'partials/content_pos_diferent-template-herramienta.php';
	}

	public function load_frontend_page_protegete_prueba() {
		require_once plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-protegete-prueba.php';
	}

	public function load_frontend_page_pymesformacion() {
		require_once plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-pymesformacion.php';
	}

	public function load_frontend_page_cursospymesmenu() {
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-financiera-cursospymesmenu.php';
	}
	public function load_frontend_page_cursosfinancieramenu() {
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-financiera-cursosfinancieramenu.php';
	}

	function infografias_shortcode( $atts, $content = null ) {
		ob_start();
		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-shortcode-infografia.php';
		return ob_get_clean();
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_pbecasintegracion() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-becas_integracion.php';
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_pemprendimientosocial() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-emprendimiento_social.php';
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_pinternoreciclaje() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-interno_reciclaje.php';
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_patrocinioaportes() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-patrocinio_aportes.php';
	}

	// Load the plugin frontend page partial.
	public function load_frontend_page_voluntariado() {
		wp_enqueue_script('fancyboxjsss', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'fanxcy', plugin_dir_url( __FILE__ ) . 'js/fancybox.js', array(), true );
		require_once plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-voluntariado_corporativo.php';
	}

	public function load_frontend_page_datosp() {
			require plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-financiera-protecciondatos.php';
		}

	public function load_frontend_page_content_financiera($atts) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'id_class'       => 'cuerpoPapa',
				'posts_per_page' => 10,
				'order'          => 'ASC',
				'post_type'      => 'financiera',
				'menu'           => 'menu-cpdf5',
				'idmenu'         => 'selector1',
			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var
		$id_class  = $atts['id_class'];
		$limite    = $atts['posts_per_page'];
		$order     = $atts['order'];
		$post_type = $atts['post_type'];
		$menu      = $atts['menu'];
		$id_menu   = $atts['idmenu'];
//		echo '<h2>' . $id_menu . '</h2>';
//		echo '<h2>' . $menu . '</h2>';
		$query = array(
			'posts_per_page'      => $limite,
			'order'               => $order,
			'post_type'           => $post_type,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
		);

		$q = new WP_Query($query);

		include plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-financiera.php';


		return ob_get_clean();
	}


	public function load_frontend_page_content_financieradatosp($atts) {
			// Attributes
			ob_start();
			$atts = shortcode_atts(
				array(
					'id_class'       => 'cuerpoPapa',
					'posts_per_page' => 10,
					'order'          => 'DSC',
					'post_type'      => 'protegete',
					'menu'           => 'cintillopdatos',
					'idmenu'         => 'selector1',
				),
				$atts,
				'new_shortcode'
			);
			// Attributes in var
			$id_class  = $atts['id_class'];
			$limite    = $atts['posts_per_page'];
			$order     = $atts['order'];
			$post_type = $atts['post_type'];
			$menu      = $atts['menu'];
			$id_menu   = $atts['idmenu'];
	//		echo '<h2>' . $id_menu . '</h2>';
	//		echo '<h2>' . $menu . '</h2>';
			$query = array(
				'posts_per_page'      => $limite,
				'order'               => $order,
				'post_type'           => $post_type,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 1,
			);

			$q = new WP_Query($query);

			include plugin_dir_path(__FILE__) . 'partials/content_post_diferent-template-protecciondatos.php';


			return ob_get_clean();
		}

	public function load_frontend_page_content_notaprensa( $atts ) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'id_class'       => 'cuerpoPapa',
				'posts_per_page' => 10,
				'order'          => 'ASC',
				//'post_type'      => 'educacionfinanciera',
				'post_type'      => 'cursos',
				'menu'           => 'menu-cpdf9',
				'idmenu'         => 'selector1',
				'ID'        	 => '302',

			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var
		$id_class    = $atts['id_class'];
		$limite    = $atts['posts_per_page'];
		$order     = $atts['order'];
		$post_type = $atts['post_type'];
		$menu      = $atts['menu'];
		$id_menu   = $atts['idmenu'];
		$ID        = $atts['ID'];

		$q = get_post($ID);



		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-notaprensa.php';


		return ob_get_clean();
	}

	public function load_frontend_page_content_pymescurso( $atts ) {
		ob_start();
		$atts = shortcode_atts(
			array(
				'post_type' => 'pymescursos',
				'id_class'  => 'cuerpoPapa',
				'menu'      => 'pymes-cursos',
				'idmenu'    => 'selector1',
				'id_cursos' => '',

			),
			$atts,
			'new_shortcode'
		);

		// Attributes in var
		$id_class  = $atts['id_class'];
		$menu      = $atts['menu'];
		$id_menu   = $atts['idmenu'];
		$ptype     = $atts['post_type'];
		$id_cursos = $atts['id_cursos'];

		$q = get_post($id_cursos);


		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-pymescursos.php';


		return ob_get_clean();
	}

	public function load_frontend_page_content_financieracurso( $atts ) {
		ob_start();
		$atts = shortcode_atts(
			array(
				'post_type' => 'cursos',
				'id_class'  => 'cuerpoPapa',
				'menu'      => 'pymes-cursos',
				'idmenu'    => 'selector1',
				'id_cursos' => '',

			),
			$atts,
			'new_shortcode'
		);

		// Attributes in var
		$id_class  = $atts['id_class'];
		$menu      = $atts['menu'];
		$id_menu   = $atts['idmenu'];
		$ptype     = $atts['post_type'];
		$id_cursos = $atts['id_cursos'];

		$q = get_post($id_cursos);


		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-cursosfinanciera.php';


		return ob_get_clean();
	}

	function load_frontend_page_content_footer( $atts ) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'post_type' => 'quienessomos',
				'menu' => 'quienes_somos',
				'orderby'   => 'date',
				'order'     => 'ASC',
			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var

		$post_type = $atts['post_type'];
		$menu = $atts['menu'];
		$orderby   = $atts['orderby'];
		$order     = $atts['order'];

		$query = array(
			'post_type'           => $post_type,
			'menu'                => $menu,
			'orderby'             => $orderby,
			'order'               => $order,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
		);

		$q     = new WP_Query( $query );

		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-footer.php';

		return ob_get_clean();

	}


	function publicidad_aprende($atts, $content = null) {
		ob_start();
		$atts_aprende = shortcode_atts(array(
			'id' => '23'
			//'direccionurl' => 'small',
		), $atts);
		//$thumbID      = get_post_thumbnail_id( $atts_aprende['id'],'bbva-image6' );

		$imgDestacada = wp_get_attachment_image_src($atts_aprende['id'], 'bbva-image6');
		//$imgDestacada = wp_get_attachment_url($thumbID);
		include plugin_dir_path(__FILE__) . 'partials/content_post_diferent-shortcode-financiera-banner.php';

		return ob_get_clean();
	}

	function publicidad_papagayo($atts, $content = null) {
		ob_start();
		$atts_aprende = shortcode_atts(array(
			'id' => '23'
			//'direccionurl' => 'small',
		), $atts);
		//$thumbID      = get_post_thumbnail_id( $atts_aprende['id'],'bbva-image6' );

		$imgDestacada = wp_get_attachment_image_src($atts_aprende['id'], 'bbva-image15');
		//$imgDestacada = wp_get_attachment_url($thumbID);
		include plugin_dir_path(__FILE__) . 'partials/content_post_diferent-shortcode-financiera-banner_papagayo.php';

		return ob_get_clean();
	}

	function load_frontend_page_content_probandopymes( $atts ) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'post_type' => 'catalogopymes',
				'orderby'   => 'date',
				'order'     => 'ASC',
			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var

		$post_type = $atts['post_type'];
		$orderby   = $atts['orderby'];
		$order     = $atts['order'];

		$query = array(
			'post_type'           => $post_type,
			'orderby'             => $orderby,
			'order'               => $order,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
		);

		$q     = new WP_Query( $query );

		include plugin_dir_path( __FILE__ ) . 'partials/content_post_diferent-template-footer0.php';

		return ob_get_clean();

	}


	// PAPAGAYO
	public function shortcode_papagayo() {
			require_once plugin_dir_path( __FILE__ ) . 'partials/papagayo_home.php';
		}


	function shortcode_videos_home( $atts ) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'color'     => 'blue',
				'limite'    => '1',
				'post_type' => 'videos',
				'orderby'   => 'date',
				'order'     => 'DESC',
				'cat'       => '',
				'slug'      => '',
			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var
		$color     = $atts['color'];
		$limite    = $atts['limite'];
		$post_type = $atts['post_type'];
		$orderby   = $atts['orderby'];
		$order     = $atts['order'];
		$cat       = $atts['cat'];
		$slug      = $atts['slug'];

		$query = array(
			'posts_per_page'      => $limite,
			'post_type'           => $post_type,
			'orderby'             => $orderby,
			'order'               => $order,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'cat'	 								=> $cat,
			'slug'	 							=> $slug,
		);

		$q     = new WP_Query( $query );
		$buton = "Saber Más";
		$n     = 0;

		include plugin_dir_path( __FILE__ ) . '/partials/videos-home.php';

		return ob_get_clean();

	}

	function shortcode_gallery( $atts ) {
		// Attributes
		ob_start();
		$atts = shortcode_atts(
			array(
				'color'         => 'black',
				'limite'        => '1',
				'cat'           => '',
				'orderby'       => 'date',
				'order'         => 'DESC',
				'longitud_desc' => '120',
			),
			$atts,
			'new_shortcode'
		);
		// Attributes in var
		$color         = $atts['color'];
		$limite        = $atts['limite'];
		$cat           = $atts['cat'];
		$orderby       = $atts['orderby'];
		$order         = $atts['order'];
		$longitud_desc = $atts['longitud_desc'];

		$query = array(
			'posts_per_page'      => $limite,
			'cat'                 => $cat,
			'orderby'             => $orderby,
			'order'               => $order,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			// 'post_type'           => 'gallery',
		);

		$q     = new WP_Query( $query );
		$buton = "Ver galería";
		$n     = 0;

		include plugin_dir_path( __FILE__ ) . '/partials/galery-home.php';


		return ob_get_clean(); 
	}


	public function register_shortcodes() {
		add_shortcode('programa_papagayo', array($this, 'load_frontend_page_papagayo'));
		add_shortcode('sala_prensa', array($this, 'load_frontend_page_saladeprensa'));
		add_shortcode('educacion_financiera', array($this, 'load_frontend_page_edufinanciera'));
		add_shortcode('educate_page', array($this, 'load_frontend_page_educate'));
		add_shortcode('herramienta', array($this, 'load_frontend_page_herramienta'));
		add_shortcode('educate', array($this, 'load_frontend_page_content_financiera'));
		add_shortcode('cursosfinanciera', array( $this, 'load_frontend_page_content_financieracurso' ));
		add_shortcode('financieracursomenu', array($this, 'load_frontend_page_cursosfinancieramenu'));
		add_shortcode('notaprensa', array( $this, 'load_frontend_page_content_notaprensa'));
		add_shortcode('pymesformacion', array( $this, 'load_frontend_page_pymesformacion' ));
		add_shortcode('infografias', array( $this, 'infografias_shortcode' ));
		add_shortcode('cursospymes', array( $this, 'load_frontend_page_content_pymescurso' ));
		add_shortcode('pymescursomenu', array($this, 'load_frontend_page_cursospymesmenu'));
		add_shortcode('becas_integracion', array($this, 'load_frontend_page_pbecasintegracion'));
		add_shortcode('emprendimiento_social', array($this, 'load_frontend_page_pemprendimientosocial'));
		add_shortcode('interno_reciclaje', array($this, 'load_frontend_page_pinternoreciclaje'));
		add_shortcode('patrocinio_aportes', array($this, 'load_frontend_page_patrocinioaportes'));
		add_shortcode('voluntariado', array($this, 'load_frontend_page_voluntariado'));
		add_shortcode('banner_papagayo', array($this, 'publicidad_papagayo'));
		add_shortcode('footer', array($this, 'load_frontend_page_content_footer'));
		add_shortcode('datosp_page', array($this, 'load_frontend_page_datosp'));
		add_shortcode('datosp', array($this, 'load_frontend_page_content_financieradatosp'));
		add_shortcode('catalogo_pymes', array($this, 'load_frontend_page_content_probandopymes'));
		add_shortcode('pagagayo_home_2', array($this, 'shortcode_papagayo'));
		add_shortcode('videos_papagayo', array($this, 'shortcode_videos_home'));
		add_shortcode('gallery_papagayo', array($this, 'shortcode_gallery'));


	}
}
