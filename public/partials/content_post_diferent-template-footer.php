<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/public/partials
 */
?>

<?php $q     = new WP_Query( array(
	'posts_per_page' => 6,
	'order'          => 'ASC',
	'post_type'      => $post_type,
	'post_status'    => 'publish'
) ); ?>



    <!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php
if (strlen($post_type)==12) {
	wp_nav_menu(
		array(
			'container'       => 'nav',
			'container_class' => 'cuerpoMenu-QSomos col-md-12 col-sm-12 col-xs-12',
			'container_id'    => 'mnu',
			'items_wrap'      => '<ul>%3$s</ul>',
			'theme_location'  => $menu
		) );
}else{
	wp_nav_menu(
		array(
			'container'       => 'nav',
			'container_class' => 'cuerpoMenu-NResponsable col-md-12 col-sm-12 col-xs-12',
			'container_id'    => 'mnu',
			'items_wrap'      => '<ul>%3$s</ul>',
			'theme_location'  => $menu
		) );
}


if ( $q->have_posts() ) :
	$contar = 0;
	$data = 'data';
?>

    <div class="cuerpoPapagayo col-md-12 col-sm-12 col-xs-12" id="cuerpopapa">
	<?php
	wp_nav_menu(
		array(
			'container'       => 'nav',
			'container_class' => 'nav-secondary',
			'container_id'    => 'selector',
			'items_wrap'      => '<ul>%3$s</ul>',
			'theme_location'  => $menu,
			'fallback_cb'     => '',
			'walker'          => new Custom_Walker(),
		) );
while ( $q->have_posts() ) :
	$q->the_post();
	$contar ++;
	$resultado= $data . $contar;
  $thumbID      = get_post_thumbnail_id( get_the_ID(), 'bbva-image14' );
	$imgDestacada = wp_get_attachment_url( $thumbID );

	$tituloInformes = rwmb_meta( 'nresponsable-text_2', get_the_ID() );
	$imagenPdf = rwmb_meta( 'nresponsable-file_advanced_2', get_the_ID() );
	$sinopsisInformes = rwmb_meta( 'nresponsable-textarea_1', get_the_ID() );
	$informesPdf = rwmb_meta( 'nresponsable-file_input_2', get_the_ID() );

	if ( $contar % 2 == 0 ) {
		?>
	    <div class="itemi col-md-12 col-sm-12 col-xs-12 menu-h" id="<?php echo $resultado ?>">
		<?php } else { ?>
	    <div class="item col-md-12 col-sm-12 col-xs-12 menu-h" id="<?php echo $resultado ?>">
		<?php } ?>
	        <img src="<?php echo $imgDestacada ?>" alt="">
	    <div>
	        <div class="est col-md-12 col-sm-12 col-xs-12">
			    <?php echo wpautop( the_content() );
					if($tituloInformes){
						for ($i=0; $i < count($tituloInformes); $i++) { ?>
							<a data-toggle="modal" data-target="#myModal<?php echo $i; ?>" href=""> <?php echo "• " . $tituloInformes[$i];?></a></br>
						<?php }
					}?>
	        </div>
	    </div>
	    </div>
		<?php endwhile; ?>
	  </div>
		<?php endif; ?>


<?php $i=0;?>
<?php if(strlen($post_type)==18){ ?>
<?php foreach($imagenPdf as $fotos){//for ($i=0; $i < count($tituloInformes) ; $i++) { ?>
			<!-- Modal -->
			<div class="modal fade" id="myModal<?php echo $i?>" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">

								<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<nav>
											<div class="navbar-header">
												<a class="navbar-brand"> Informes de Banca Responsable</a>
											</div>

										</nav>
								</div>
								<div class="modal-body" style="overflow:hidden">
											<?php $foto = wp_get_attachment_image_src($fotos['ID'], 'bbva-programasmodal' );
											$src = $foto[0];?>

											<div class="img-modal-body col-md-5 col-sm-5 col-xs-5">
												<img src="<?php echo $src?>" alt="" style="margin-top: 4%; height: 18em; position: fixed; width: 37%;">
											</div>

											<div class="cuerpo-modal-body col-md-7 col-sm-7 col-xs-7">
												<p style="font-weight:bold">Informe Anual de Banca Responsable <?php echo $tituloInformes[$i];?></p>
												<p><?php echo $sinopsisInformes[$i]?></p>
											</div>

								</div>

								<div class="modal-footer">
								<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
									<div class="control">
										<div class="btn-group botonesCompartirInfografia  buttonPrograma" style="top: -10px; right: 15%;">
											<a class="btn-default dropdown-toggle buttonCompartir" data-toggle="dropdown"><i class="fa fa-share-alt-square" aria-hidden="true"></i>  Compartir</a>
											<ul class="dropdown-menu1" role="menu" style="float:right !important; margin: 26px -95px 0 0; margin-left: -70px;">
												<li><a style="padding: 0 10px 0 0;"href="https://www.facebook.com/sharer/sharer.php?u=<?php 	the_permalink();?>" class="facebook" target="_blank"> <i style="color:#095798; font-size:22px;" class="fa fa-facebook-square"></i></a></li>
												<li><a style="padding: 0 10px 0 0;" href="https://twitter.com/intent/tweet?url=<?php the_permalink();?>&text=<?php the_title_attribute(); ?>%20por%20@ayudawp" class="twitter" target="_blank"> <i style="color:#76a9da; font-size:22px;" class="fa fa-twitter-square"></i></a></li>
												<li><a style="padding: 0 10px 0 0;" href="https://plus.google.com/share?url=<?php the_permalink();?>" class="googleplus" target="_blank"> <i style="color:#dc4a3e; font-size:22px" class="fa fa-google-plus-square"></i></a></li>
											</ul>
											<a class="buttonDescargar" href="<?php echo $informesPdf[$i];?>" download="<?php echo the_title();?>"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i>  Descargar</a>
										</div>
									</div>
								</div>
					</div>
				</div>
			</div>
<?php $i++;} }?>


<div class="cuerpoMenuResponsive col-md-12 col-sm-12 col-xs-12">
<?php
$menu1      = 'menu-cpdf1';
$locations = get_nav_menu_locations();
$menu      = wp_get_nav_menu_object( $locations[ $menu1 ] );
$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
$i=0;
$contar2 =0;
foreach ( $menuitems as $menu_item ) {
    $arrayitem[] = $menu_item->title;
}
if ( $q->have_posts() ) :
	while ( $q->have_posts() ) : $q->the_post();
        $contar2 ++;
        $meta_print_boton1 = get_post_meta( get_the_ID(), 'botn_texto', true );
        $meta_print_boton3 = get_post_meta( get_the_ID(), 'botn_texto2', true );
		$meta_print_url1   = get_post_meta( get_the_ID(), 'botn_url', true );
        $imggal3 = rwmb_meta( 'video-text_1', get_the_ID() );
        $imggal4 = rwmb_meta( 'video-text_2', get_the_ID() );
        ?>
		<div class="borde">
            <h2><?php echo $arrayitem[$i] ?>
                <span class="fa fa-chevron-down"></span>
            </h2>
        <?php if ( $contar2 % 2 == 0 ) { ?>
		<div class="plegable-1" id="data<?php echo $contar2 ?>">
		<?php } else { ?>
		<div class="plegable" id="data<?php echo $contar2 ?>">
		<?php } ?>
			   <?php echo wpautop( the_content() ); ?>
			   <?php if (isset($imggal3)) { ?>
	                       <?php for ($k=0; $k<count($imggal3); $k++) { ?>
                                <a  data-fancybox="gallery-3" data-type="iframe"
                                    data-src="https://www.youtube.com/embed/<?php echo $imggal3[$k]?>"
                                    href="javascript:;"
                                >
			                    <?php echo $imggal4[$k] ?>
                                </a> <br>
	                       <?php } ?>
	           <?php } ?>
			   <?php if ( $meta_print_boton1 ) { ?>
			   <div class="bot-plega">
                  <a <?php if ($meta_print_boton3) {?> data-toggle="modal" data-target="#myModal"<?php }?>
                     href=" <?php echo $meta_print_url1 ?>">
					 <?php echo $meta_print_boton1 ?></a>
               </div>
					<?php } ?>
        </div>
        </div>
		<?php $i++; endwhile;?>
	</div>
     <?php
endif;
