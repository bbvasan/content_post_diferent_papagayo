<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       content_post_diferent
 * @since      1.0.0
 *
 * @package    Content_post_diferent
 * @subpackage Content_post_diferent/public/partials
 */
?>

<?php $q     = new WP_Query( array(
	'posts_per_page' => 1,
	'order'          => 'DESC',
	'post_type'      => $post_type,
	'post_status'    => 'publish'
) ); ?>



    <!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php
if ( $q->have_posts() ) :
	$contar = 0;
	$data = 'data';
?>
    <div class="CursoPymes col-md-12">
	<?php
		while ( $q->have_posts() ) :
			$q->the_post(); $con++;
			$catalogo = rwmb_meta( 'prefix-pymespruebatext_1', get_the_ID() );
			$urlcatalogo = rwmb_meta( 'prefix-pymespruebatext_2', get_the_ID() );?>
			<div class="CursoPymesTexto col-md-5">
				<h3><?php echo the_title() ?></h3>
				<?php echo wpautop(the_content());?>
				<?php if ($catalogo){?>
					<a href="<?php echo $urlcatalogo?>"><button><?php echo $catalogo?></button></a>
				<?php } ?>
			</div>
			<div class="CursoPymesImagen col-md-7">
				<?php $foto = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'bbva-image10' );$src = $foto[0]; ?>
				<img src="<?php echo $src ?>" alt="">
			</div>
		<?php endwhile; ?>
	  </div>
		<?php endif; ?>