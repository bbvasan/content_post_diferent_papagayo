<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       LuisCapote
 * @since      1.0.0
 *
 * @package    Noticias_bbva
 * @subpackage Noticias_bbva/public/partials
 */
?>
<?php

 $q     = new WP_Query( array(
 	'posts_per_page' => 1,
 	'order'          => 'DESC',
 	'post_type'      => 'videos',
 	'post_status'    => 'publish'
 ) );

if ( $q->have_posts() ) :
		while ( $q->have_posts() ) :
			$q->the_post(); 
			$foto = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'bbva-home2' );
    		$src = $foto[0];
			?>
      	<div class="w3-card-s">
	      	
			<a href="<?php echo get_post_type_archive_link('videos')?>"><img src="<?php echo $src ?>" style="width:100%; height: 22.67em;"></a>
          <header class="w3-container-f">

          	<a href="<?php echo get_post_type_archive_link('videos')?>"> <h3><?php echo get_the_title(); ?></h3> </a>
          </header>
        </div>
		<?php endwhile;
		wp_reset_query();
endif; ?>


